/*
 * CollisionDetector.cpp
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan
 */
#include "Arduino.h"
#include "CollisionDetector.h"

CollisionDetector::CollisionDetector(int digitalPin, int minFator, int fator) {
	this->digitalPin = digitalPin;
	this->minFator = minFator;
	this->maxFator = fator;
	this->fator = 9999;
}

CollisionDetector::~CollisionDetector() {

}

bool CollisionDetector::willItCollide() {
	return false; //this->fator <= this->minFator;
}

void CollisionDetector::setup() {

}

void CollisionDetector::test() {

}

void CollisionDetector::loop() {
	int valor = analogRead(this->digitalPin);
	this->fator = map(valor, 0, 1023, 1, this->maxFator);
}
