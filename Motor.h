/*
 * Motor.h
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan C�ndido <brodao@gmail.com>
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "arduino.h"

class Motor {
public:
	Motor(int pin1, int pin2, int speed);
	virtual ~Motor();

	void test();
	void setup();
	void loop();

	void setSpeed(int speed);
	int getSpeed();

	void stop();
	void backward();
	void forward();

	float correcao;
private:
	int pin1;
	int pin2;
	int pin3;
	int speed;
	int sinal1;
	int sinal2;
};

#endif /* MOTOR_H_ */
