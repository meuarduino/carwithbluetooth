/*
 * Motor.cpp
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan C�ndido <brodao@gmail.com>
 */
#include "Arduino.h"
#include "Motor.h"
#include "CollisionDetector.h"
#include "Horn.h"
#include "BlueTooth.h"

#define MOTOR_A_P1    9
#define MOTOR_A_P2    8
#define MOTOR_A_SPD  10

#define MOTOR_B_P1   5
#define MOTOR_B_P2   4
#define MOTOR_B_SPD  6

#define HORN 12

#define COLLISION_SENSOR PIN_A0

const char CMD_HORN = 'H';
const char CMD_MELODY = 'M';

const char CMD_BACKWARD = '-';
const char CMD_FORWARD = '+';

const char CMD_VELOCITY = 'V';

const char CMD_DIRECTION = 'D';
const char CMD_DIRECTION_A = 'A';
const char CMD_DIRECTION_B = 'B';
const char CMD_DIRECTION_C = 'C';
const char CMD_DIRECTION_D = 'D';
const char CMD_DIRECTION_E = 'E';

const char CMD_DEMO = 'X';
const char CMD_WAIT = 'W';

Motor* motorA;
Motor* motorB;

CollisionDetector* cd;

Horn* horn;

BlueTooth* bt;

int saveSpeed[] = { 0, 0 };

void setup() {
	bt = new BlueTooth();
	bt->setup();
	bt->test();

	bt->starting();

	motorA = new Motor(MOTOR_A_P1, MOTOR_A_P2, MOTOR_A_SPD);
	motorA->setup();
	motorA->test();
	motorA->correcao = 0.45;

	motorB = new Motor(MOTOR_B_P1, MOTOR_B_P2, MOTOR_B_SPD);
	motorB->setup();
	motorB->test();

	cd = new CollisionDetector(COLLISION_SENSOR, 1, 5);
	cd->setup();
	cd->test();

	horn = new Horn(HORN);
	horn->setup();
	horn->test();

	bt->ready();

	//bt->wait();
	horn->loop();
}

bool run = true;

void loop() {
	cd->loop();

	if (cd->willItCollide()) {
		bt->collision();

		motorA->stop();
		motorB->stop();
		motorA->loop();
		motorB->loop();
	};

	bt->loop();
	String data = bt->getData();
	if (!data.equals("")) {
		bt->log(LOG_INFO, "Comando");
		bt->log(LOG_INFO, data);
		Serial.println();
	}

	while (!data.equals("")) {
		char c = data.charAt(0);
		int pos = 1;

		if (c == CMD_WAIT) {
			delay(2000);
		} if (c == CMD_DEMO) {
			bt->log(LOG_INFO, "Modo DEMO");
			bt->log(LOG_INFO, "Fim quando buzinar");

			data.concat("SWWV5+WV6WV7WV8WV9WDAWDEWDBWDDWDCWW");
			data.concat("SWV5-WV6WV7WV8WV9WDAWDEWDBWDDWDCWWSHH");
		} else if (c == CMD_VELOCITY) {
			c = data.charAt(1);
			pos = 2;
			int speed = map((int) c, (int) '0', (int) '9', 0, 255);

			motorA->setSpeed(speed);
			motorB->setSpeed(speed);

			saveSpeed[0] = speed;
			saveSpeed[1] = speed;

			bt->log(LOG_INFO, "Velocidade ");
			bt->log(LOG_INFO, speed);
		} else if (c == CMD_DIRECTION) {
			int direction[] = { 0, 0 };

			c = data.charAt(1);
			pos = 2;

			if ((c >= CMD_DIRECTION_A) && (c <= CMD_DIRECTION_E)) {
				if (c == CMD_DIRECTION_A) {  // 9h
					direction[0] = saveSpeed[0];
					direction[1] = 0;
				} else if (c == CMD_DIRECTION_B) {  // 10h30
					direction[0] = saveSpeed[0];
					direction[1] = saveSpeed[1] * 0.5;
				} else if (c == CMD_DIRECTION_C) {  // 0h
					direction[0] = saveSpeed[0];
					direction[1] = saveSpeed[1];
				} else if (c == CMD_DIRECTION_D) {  // 2h30
					direction[0] = saveSpeed[0] * 0.5;
					direction[1] = saveSpeed[1];
				} else if (c == CMD_DIRECTION_E) {  // 3h
					direction[0] = 0;
					direction[1] = saveSpeed[1];
				}

				motorA->setSpeed(direction[0]);
				motorB->setSpeed(direction[1]);

				bt->log(LOG_INFO, "Direcao ");
				bt->log(LOG_INFO, direction[0]);
				bt->log(LOG_INFO, direction[1]);
			} else {
				bt->log(LOG_ERRO, "Direcao inv�lida");
				bt->log(LOG_ERRO, c);

			}
		} else if (c == CMD_FORWARD) {
			motorA->forward();
			motorB->forward();

			bt->log(LOG_INFO, "Para frente");
		} else if (c == CMD_BACKWARD) {
			motorA->backward();
			motorB->backward();

			bt->log(LOG_INFO, "Para tras");
		} else if (c == 'S') {
			motorA->stop();
			motorB->stop();

			bt->log(LOG_INFO, "Parado");
		} else if (c == CMD_MELODY) {

			bt->log(LOG_INFO, "Armazena melodia");
		} else if (c == CMD_HORN) {
			horn->loop();

			bt->log(LOG_INFO, "Buzina");
		}

		motorA->loop();
		motorB->loop();

		data = data.substring(pos);
	}

	delay(100);
}
