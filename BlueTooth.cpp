/*
 * BlueTooth.cpp
 *
 *  Created on: 10 de fev de 2018
 *      Author: Alan
 */

#include "BlueTooth.h"

#define BT_STATE 2

BlueTooth::BlueTooth() {
	// TODO Auto-generated constructor stub

}

BlueTooth::~BlueTooth() {

}

void BlueTooth::setup() {
	Serial.begin(9600);

	pinMode(BT_STATE, INPUT);
}

void BlueTooth::test() {

}

void BlueTooth::loop() {
	data = "";

//	if (digitalRead(BT_STATE) == LOW) { //perdeu conex�o
//		data = "S"; // for�a parada do carro
//	} else if (Serial.available() > 0) {
		data = read();
//	}
}

String BlueTooth::getData() {
	return data;
}

void BlueTooth::log(int code) {
	write(code);
	write('\n');
}

void BlueTooth::log(int code, String data) {
	write(code);
	write(data);
	write('\n');
}

void BlueTooth::log(int code, int data) {
	write(code);
	write(data);
	write('\n');
}

void BlueTooth::starting() {
	log(LOG_START);
}

void BlueTooth::ready() {
	log(LOG_READY);
}

void BlueTooth::wait() {
	while (read() != "OK") {
		delay(500);
		log(LOG_WAIT);
	}
}

void BlueTooth::collision() {
	log(LOG_COLLISION);
}

String BlueTooth::read() {
	return Serial.readStringUntil('\n');
}

void BlueTooth::write(String &data) {
	Serial.print(data);
}

void BlueTooth::write(int data) {
	Serial.print(data);
}

void BlueTooth::write(char data) {
	Serial.print(data);
}
