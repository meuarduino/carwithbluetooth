/*
 * Motor.cpp
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan C�ndido <brodao@gmail.com>
 */

#include "Motor.h"

Motor::Motor(int pin1, int pin2, int pin3) {
	this->pin1 = pin1;
	this->pin2 = pin2;
	this->pin3 = pin3;

	this->sinal1 = LOW;
	this->sinal2 = LOW;
	this->speed = 0;

	this->correcao = 1;
}

Motor::~Motor() {
	stop();
}

void Motor::test() {
	setSpeed(128);
	forward();
	loop();
	backward();
	loop();
	stop();
	loop();
}

void Motor::setSpeed(int speed) {
	if (speed < 0) {
		this->speed = 0;
	} else if (speed > 255) {
		this->speed = 255;
	} else {
		this->speed = speed;
	}
}
int Motor::getSpeed() {
	return this->speed;
}

void Motor::stop() {
	this->sinal1 = HIGH;
	this->sinal2 = HIGH;
}

void Motor::backward() {
	this->sinal1 = LOW;
	this->sinal2 = HIGH;
}

void Motor::forward() {
	this->sinal1 = HIGH;
	this->sinal2 = LOW;
}

void Motor::setup() {
	pinMode(this->pin1, OUTPUT);
	pinMode(this->pin2, OUTPUT);
	if (this->pin3 > 0) {
		pinMode(this->pin3, OUTPUT);
	}

	setSpeed(0);
	stop();
}

void Motor::loop() {
	if (this->pin3 > 0) {
		analogWrite(this->pin3, this->speed*this->correcao);
	}

	digitalWrite(this->pin1, this->sinal1);
	digitalWrite(this->pin2, this->sinal2);
}

