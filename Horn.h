/*
 * Horn.h
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan
 */

#ifndef HORN_H_
#define HORN_H_

class Horn {
public:
	Horn(int pin1);
	virtual ~Horn();

	void test();
	void setup();
	void loop();

	void setMelody(int size, int melody[], int duration[]);

private:
	int pin1;
	int* melody;
	int* duration;
	int melodysize;
};

#endif /* HORN_H_ */
