/*
 * CollisionDetector.h
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan
 */

#ifndef COLLISIONDETECTOR_H_
#define COLLISIONDETECTOR_H_

#include "Arduino.h"

class CollisionDetector {
public:
	CollisionDetector(int digitalPin, int minFator, int fator);
	virtual ~CollisionDetector();

	bool willItCollide();

	void setup();
	void test();
	void loop();
private:
	int digitalPin;
	int fator;
	int minFator;
	int maxFator;
};

#endif /* COLLISIONDETECTOR_H_ */
