/*
 * Horn.cpp
 *
 *  Created on: 9 de fev de 2018
 *      Author: Alan
 */
#include "Arduino.h"
#include "Horn.h"

Horn::Horn(int pin1) {
	this->pin1 = pin1;

	this->melody = 0;
	this->duration = 0;
	this->melodysize = 0;
}

Horn::~Horn() {

}

#define FREQUENCY 300  // Hz
#define PERIOD 500000L / FREQUENCY  // (1 / frequency) * 1e6 / 2

void Horn::setMelody(int size, int melody[], int duration[]) {
	if (this->melody != 0) {
		delete[] this->melody;
		delete[] this->duration;
	}

	this->melody = new int[size+1];
	this->duration = new int[size+1];

	for (int i = 0; i < size; i++) {
		this->melody[i] = melody[i];
		this->duration[i] = duration[i];
	}

	this->melodysize = size;
}

#define NOTE_C4  262 // C
#define NOTE_CS4  277 // C#
#define NOTE_D4  294 // D
#define NOTE_DS4  311 // D#
#define NOTE_E4  330 // E
#define NOTE_F4  349 // F
#define NOTE_FS4  370 // F#
#define NOTE_G4  392 // G
#define NOTE_GS4  415 // G#
#define NOTE_A4  440 // A
#define NOTE_AS4  466 // A#
#define NOTE_B4  494 // B
#define NOTE_C5  523 // C uma oitava acima

void Horn::setup() {
	int melody[13] = {
	NOTE_C4,
	NOTE_CS4,
	NOTE_D4,
	NOTE_DS4,
	NOTE_E4,
	NOTE_F4,
	NOTE_FS4,
	NOTE_G4,
	NOTE_GS4,
	NOTE_A4,
	NOTE_AS4,
	NOTE_B4,
	NOTE_C5 };

	int duration[] = { 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 };

	pinMode(this->pin1, OUTPUT);

	this->setMelody(13, melody, duration); //buzina de teste
}

void Horn::loop() {
//	// to calculate the note duration, take one second divided by the note type.
//	//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
//	int noteDuration = 1000 / noteDurations[thisNote];
//	tone(buzzerPin, melody[thisNote], noteDuration);
//
//	// to distinguish the notes, set a minimum time between them.
//	// the note's duration + 30% seems to work well:
//	int pauseBetweenNotes = noteDuration * 1.30;
//	delay(pauseBetweenNotes);
//	// stop the tone playing:
//	noTone(buzzerPin);

	for (int i = 0; i < this->melodysize; ++i) {
		int noteDuration = 1000 / this->duration[i];
		tone(this->pin1, this->melody[i], noteDuration);
		int pauseBetweenNotes = noteDuration * 1.30;
		delay(pauseBetweenNotes);
		noTone(this->pin1);
	}

}

void Horn::test() {
	loop();
}
