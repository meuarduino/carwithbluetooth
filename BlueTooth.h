/*
 * BlueTooth.h
 *
 *  Created on: 10 de fev de 2018
 *      Author: Alan
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "Arduino.h"

#define LOG_START     0x01
#define LOG_READY     0x02
#define LOG_WAIT      0x03
#define LOG_COLLISION 0x04
#define LOG_INFO      0xF0
#define LOG_ERRO      0xF1

class BlueTooth {
public:
	BlueTooth();
	virtual ~BlueTooth();

	void setup();
	void test();
	void loop();

	void log(int code);
	void log(int code, String data);
	void log(int code, int data);

	void starting();
	void ready();

	void wait();

	void collision();

	String getData();
private:
	String read();
	void write(String &data);
	void write(int data);
	void write(char data);

	String data;
};

#endif /* BLUETOOTH_H_ */
